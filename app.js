var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io').listen(server);
var defaultPort = 8080;
var playerList = [];

app.use('/core', express.static(__dirname + '/core'));
app.use('/js', express.static(__dirname + '/js'));
app.use('/css', express.static(__dirname + '/css'));
app.use('/assets', express.static(__dirname + '/assets'));

// Game Client
app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

// Server Handling
server.listen(process.env.PORT || defaultPort, function() {
  console.log('Listening on ' + server.address().port);
});

server.playerID = 0;
// Server Sockets
io.on('connection',function(socket){

  socket.on('newplayer',function(){
    socket.player = { id: server.playerID++, x:32, y:32 };
    socket.emit('newplayer', socket.player);

    socket.on('disconnect',function(id){
      delete playerList[id];
      io.emit('remove', socket.player.id);
    });

    socket.on('updateServer',function(data){
      playerList[socket.player.id] = data;
      setInterval(function () {
        if (getAllPlayers().length > 0) {
          socket.emit('allplayers', playerList);
        }
      }, 100);
    });
  });

  socket.on('test',function(){
    console.log('Socket Test Received From Client');
  });

});

function getAllPlayers(){
  var players = [];
  // var epoch = (new Date).getTime();
  Object.keys(io.sockets.connected).forEach(function(socketID){
    var player = io.sockets.connected[socketID].player;
    // player.time = epoch;
    if(player) players.push(player);
  });
  return players;
}
