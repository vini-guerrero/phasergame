/**
 * Created by Vinicius Guerrero (vini.ts.guerrero@gmail.com)
*/

var Client = {};
Client.socket = io.connect();

Client.sendTest = function(){
    console.log("Socket Test Sent to Server");
    Client.socket.emit('test');
};

Client.requestNewPlayer = function(){
  Client.socket.emit('newplayer');
};

Client.updatePosition = function(player){
  var x = Game.map.worldToTileX(player.x);
  var y = Game.map.worldToTileY(player.y);
  Client.socket.emit('updatePosition',{x:x,y:y});
};

Client.updatePositionClick = function(x, y){
  Client.socket.emit('updatePosition',{x:x,y:y});
};

Client.socket.on('newplayer',function(data){
  var gamerId = data.id
  Game.createPlayer(gamerId);
  Game.addPlayerControls(gamerId);
  var player = Game.player;

  setInterval(function() {
    var position = { id: gamerId, x: player.x, y: player.y };
    if (player.x != data.x || player.y != data.y) {
      Client.socket.emit('updateServer', position);
    }
  }, 100);
});

Client.socket.on('allplayers',function(data){
    Game.playerMap = data;
});

Client.socket.on('remove',function(gamerId){
    // Game.removePlayer(gamerId);
});
