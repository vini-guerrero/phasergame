Game.createPlayer = function(gamerId){
  var scene = Game.scene;
  Game.player = Game.playerMap[gamerId];
  Game.player = scene.add.sprite(32,32,'phaserguy');
  Game.player.setDepth(1);
  Game.player.setOrigin(0,0.5);
  Game.camera.startFollow(Game.player);
}

Game.addPlayerControls = function (gamerId){
  // Keyboard Keys
  var scene = Game.scene;
  scene.keyW = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
  scene.keyS = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
  scene.keyD = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
  scene.keyA = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
  // Mouse Clicks
  // Handles the clicks on the map to make the character move
  scene.update = function(){
    var player = Game.player;
    if (game.input.activePointer.isDown){
      Game.handleClick(game.input.activePointer, gamerId);
    }
    if (scene.keyS.isDown) {
      var tileIndex = Game.map.getTileAtWorldXY(player.x, player.y + 28, true).index;
      if (tileIndex === 1){
        player.y += playerKeySpeed;
        Client.updatePosition(player);
      }
    }
    if (scene.keyW.isDown){
      var tileIndex = Game.map.getTileAtWorldXY(player.x, player.y - 12, true).index;
      if (tileIndex === 1){
        player.y -= playerKeySpeed;
        Client.updatePosition(player);
      }
    }
    if (scene.keyD.isDown){
      var tileIndex = Game.map.getTileAtWorldXY(player.x + 30, player.y, true).index;
      if (tileIndex === 1){
        player.x += playerKeySpeed;
        Client.updatePosition(player);
      }
    }
    if (scene.keyA.isDown){
      var tileIndex = Game.map.getTileAtWorldXY(player.x - 4, player.y, true).index;
      if (tileIndex === 1){
        player.x -= playerKeySpeed;
        Client.updatePosition(player);
      }
    }
  }
  // Marker that will follow the mouse
  // Game.createMouseMarker();
}

Game.moveCharacter = function(path, gamerId){
  // Sets up a list of tweens, one for each tile to walk, that will be chained by the timeline
  var player = Game.player;
  var tweens = [];
  for(var i = 0; i < path.length-1; i++){
    var ex = path[i+1].x;
    var ey = path[i+1].y;
    tweens.push({
      targets: player,
      x: {value: ex*Game.map.tileWidth, duration: playerClickSpeed},
      y: {value: ey*Game.map.tileHeight, duration: playerClickSpeed}
    });
  }
  Game.scene.tweens.timeline({tweens: tweens});
  var moveLength = path.length -1;
  Client.updatePositionClick(path[moveLength].x, path[moveLength].y);
};

Game.handleClick = function(pointer, gamerId){
  var player = Game.player;
  var x = Game.camera.scrollX + pointer.position.x;
  var y = Game.camera.scrollY + pointer.position.y;
  var toX = Math.floor(x/32);
  var toY = Math.floor(y/32);
  var fromX = Math.floor(player.x/32);
  var fromY = Math.floor(player.y/32);
  Game.finder.findPath(fromX, fromY, toX, toY, function( path ) {
    if (path !== null) Game.moveCharacter(path, gamerId);
  });
  Game.finder.calculate(); // Don't Forget, Otherwise Nothing Happens
};

Game.createMouseMarker = function(){
  var scene = Game.scene;
  var marker;
  marker = scene.add.graphics();
  marker.lineStyle(3, 0xffffff, 1);
  marker.strokeRect(0, 0, Game.map.tileWidth, Game.map.tileHeight);
  Game.marker = marker;
  scene.update = function(){
    var worldPoint = this.input.activePointer.positionToCamera(this.cameras.main);
    // Rounds Down to Nearest Tile
    var pointerTileX = Game.map.worldToTileX(worldPoint.x);
    var pointerTileY = Game.map.worldToTileY(worldPoint.y);
    Game.marker.x = Game.map.tileToWorldX(pointerTileX);
    Game.marker.y = Game.map.tileToWorldY(pointerTileY);
  }
}

Game.removePlayer = function(gamerId){
  Game.playerMap[gamerId].destroy();
  delete Game.playerMap[gamerId];
}

Game.movePlayer = function(data){
  // interpolateEntity();
  var player = Game.playerMap[data.id];
  var tileX = Game.map.tileToWorldX(data.x);
  var tileY = Game.map.tileToWorldY(data.y);
  player.x = tileX;
  player.y = tileY;
};
  
// function interpolateEntity() {
//
//   var epoch = (new Date).getTime();
//   var updates = Game.updates;
// 	// Assuming currentTime is synced with server time
// 	// Calculate time at which we want to render the entity
// 	var time = epoch - 0.2; //200 ms in the past
//
// 	// Find points that surround our time
// 	var i = 0;
//   while (updates[i].time < time) {
//     console.log("1111");
//   }
// 	while(updates[i].time < time) i++;
// 	var before = updates[i-1];
// 	var after = updates[i];
//
// 	// Calculate Progress Towards 'after'
// 	var alpha = (time - before.time) / (after.time - before.time);
//
// 	// Interpolate
//   var result = {
//     x: before.x + (after.x - before.x) * alpha,
//     y: before.y + (after.y - before.y) * alpha
//   }
//   console.log(result);
// 	return result;
// }
