/**
 * Created by Vinicius Guerrero (vini.ts.guerrero@gmail.com)
*/
var config = {
    type: Phaser.AUTO,
    width: 20*64,
    height: 20*32,
    parent: 'game',
    scene: [Game]
};

var game = new Phaser.Game(config);
var gamerTag = "Admin";
var gamerId;
var playerClickSpeed = 200;
var playerKeySpeed = 6;
