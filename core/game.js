/**
 * Created by Vinicius Guerrero (vini.ts.guerrero@gmail.com)
*/
var Game = {};
// Game Initialize
// Holds Player List Array Object
Game.playerMap = {}
Game.updates = [];
Game.player;

Game.preload = function(){
    Game.scene = this; // Handy reference to the scene (alternative to `this` binding)
    this.load.image('tileset', 'assets/gridtiles.png');
    this.load.tilemapTiledJSON('map', 'assets/map.json');
    this.load.image('phaserguy', 'assets/phaserguy.png');
};

Game.create = function(){

    // Player Settings
    Client.requestNewPlayer();

    this.input.keyboard.on('keydown_ENTER', function (event) {
      Client.sendTest();
    });
    // Camera Settings
    Game.camera = this.cameras.main;
    Game.camera.setBounds(0, 0, 20*64, 20*32);
    // Game.camera.follow(player);

    // Display map
    Game.map = Game.scene.make.tilemap({ key: 'map'});
    // The first parameter is the name of the tileset in Tiled and the second parameter is the key
    // of the tileset image used when loading the file in preload.
    var tiles = Game.map.addTilesetImage('tiles', 'tileset');
    Game.map.createStaticLayer(0, tiles, 0,0);

    // ### PathFinding ###
    // Initializing the pathfinder
    Game.finder = new EasyStar.js();

    // We create the 2D array representing all the tiles of our map
    var grid = [];
    for(var y = 0; y < Game.map.height; y++){
        var col = [];
        for(var x = 0; x < Game.map.width; x++){
            // In each cell we store the ID of the tile, which corresponds
            // to its index in the tileset of the map ("ID" field in Tiled)
            col.push(Game.getTileID(x,y));
        }
        grid.push(col);
    }
    Game.finder.setGrid(grid);

    var tileset = Game.map.tilesets[0];
    var properties = tileset.tileProperties;
    var acceptableTiles = [];

    // We need to list all the tile IDs that can be walked on. Let's iterate over all of them
    // and see what properties have been entered in Tiled.
    for(var i = tileset.firstgid-1; i < tiles.total; i++){ // firstgid and total are fields from Tiled that indicate the range of IDs that the tiles can take in that tileset
        if(!properties.hasOwnProperty(i)) {
            // If there is no property indicated at all, it means it's a walkable tile
            acceptableTiles.push(i+1);
            continue;
        }
        if(!properties[i].collide) acceptableTiles.push(i+1);
        if(properties[i].cost) Game.finder.setTileCost(i+1, properties[i].cost); // If there is a cost attached to the tile, let's register it
    }
    Game.finder.setAcceptableTiles(acceptableTiles);
};

Game.update = function(){

    if (Game.marker){
      var worldPoint = this.input.activePointer.positionToCamera(this.cameras.main);
      var pointerTileX = Game.map.worldToTileX(worldPoint.x);
      var pointerTileY = Game.map.worldToTileY(worldPoint.y);
      if (Game.map.getTileAt(pointerTileX, pointerTileY)){
        Game.marker.setVisible(!Game.checkCollision(pointerTileX,pointerTileY));
      }
    }
};

Game.checkCollision = function(x,y){
    var tile = Game.map.getTileAt(x, y);
    return tile.properties.collide == true;
};

Game.getTileID = function(x,y){
    var tile = Game.map.getTileAt(x, y);
    return tile.index;
};
